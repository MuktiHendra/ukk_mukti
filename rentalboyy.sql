-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 23 Mar 2022 pada 10.34
-- Versi server: 10.4.17-MariaDB
-- Versi PHP: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ukk`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `nama_admin` varchar(120) NOT NULL,
  `username` varchar(120) NOT NULL,
  `password` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `nama_admin`, `username`, `password`) VALUES
(1, 'mukti', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `customer`
--

CREATE TABLE `customer` (
  `id_customer` int(11) NOT NULL,
  `nama` varchar(120) NOT NULL,
  `username` varchar(120) NOT NULL,
  `alamat` varchar(120) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `no_telepon` varchar(20) NOT NULL,
  `no_ktp` varchar(50) NOT NULL,
  `password` varchar(120) NOT NULL,
  `role_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `customer`
--

INSERT INTO `customer` (`id_customer`, `nama`, `username`, `alamat`, `gender`, `no_telepon`, `no_ktp`, `password`, `role_id`) VALUES
(4, 'Joko Santoso', 'joko', 'Jl. Satu Pekanbaru', 'laki-laki', '0653246512', '215654532767', '9ba0009aa81e794e628a04b51eaf7d7f', 2),
(6, 'Andi', 'andi', 'Jakarta', 'laki-laki', '0217687634', '12747657463', '827ccb0eea8a706c4c34a16891f84e7b', 2),
(7, 'Arif', 'admin', 'Pekanbaru', 'laki-laki', '065423624', '1764578345', '21232f297a57a5a743894a0e4a801fc3', 1),
(8, 'Bayu', 'bayu', 'Jl. Nangka Pekanbaru', 'laki-laki', '07612233', '14000756764735', '81dc9bdb52d04dc20036dbd8313ed055', 2),
(10, 'adelia', 'adeng', 'jalan kalimas baru', 'perempuan', '081907428169', '35781', '35dd12094c8ca8912833ec25522565bc', 2),
(11, 'rafa', 'rafa', 'jalan kalimas bau 3 ', 'laki-laki', '081907428169', '35781', '35cd2d0d62d9bc5e60a3ca9f7593b05b', 2),
(12, 'pandu', 'pandu', 'Jl kalimas baru tiga gg 15 no 9', 'laki-laki', '081907428169', '35781', '8acf7115033fa7bbfebe4b9b726ab374', 2),
(13, 'novan', 'emos', 'jl kedurus', 'perempuan', '08882011001223', '1010101', 'f6f1eba5b7edc39b6ba2ebd4343a744c', 0),
(14, 'Mukti', 'admin', 'Jl kalimas baru tiga gg 15 no 9', 'perempuan', '081907428169', '35781', '636a002617548dce950e4e94f5a7e32b', 0),
(16, 'opal ganteng', 'gay', 'gay', 'perempuan', '08882011001223', '10020120', 'gay', 0),
(17, 'devan', 'linda', 'jalan simokerto', 'laki-laki', '081907428169', '1010101', '827ccb0eea8a706c4c34a16891f84e7b', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `rental`
--

CREATE TABLE `rental` (
  `id_rental` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_sepeda` int(11) NOT NULL,
  `tgl_rental` date NOT NULL,
  `tgl_kembali` date NOT NULL,
  `harga` varchar(120) NOT NULL,
  `denda` varchar(120) NOT NULL,
  `tgl_pengembalian` date DEFAULT NULL,
  `status_pengembalian` varchar(50) NOT NULL,
  `status_rental` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `rental`
--

INSERT INTO `rental` (`id_rental`, `id_customer`, `id_sepeda`, `tgl_rental`, `tgl_kembali`, `harga`, `denda`, `tgl_pengembalian`, `status_pengembalian`, `status_rental`) VALUES
(5, 4, 28, '2022-03-15', '2022-03-30', '', '2000', NULL, 'Belum Kembali', 'Belum Selesai'),
(6, 4, 28, '2022-03-16', '2022-03-19', '', '2000', NULL, 'Belum Kembali', 'Belum Selesai'),
(7, 4, 28, '2022-03-19', '2022-03-21', '', '2000', NULL, 'Belum Kembali', 'Belum Selesai'),
(8, 4, 30, '2022-03-19', '2022-03-31', '', '4500', NULL, 'Belum Kembali', 'Belum Selesai'),
(9, 4, 28, '2022-03-02', '2022-03-11', '', '2000', '0000-00-00', 'Belum Kembali', 'Belum Selesai'),
(10, 4, 28, '2022-03-02', '2022-03-11', '', '2000', '0000-00-00', 'Belum Kembali', 'Belum Selesai'),
(11, 4, 28, '2022-03-02', '2022-03-11', '', '2000', '0000-00-00', 'Belum Kembali', 'Belum Selesai'),
(12, 4, 28, '2022-03-02', '2022-03-11', '', '2000', '0000-00-00', 'Belum Kembali', 'Belum Selesai'),
(13, 4, 28, '2022-03-02', '2022-03-11', '', '2000', '0000-00-00', 'Belum Kembali', 'Belum Selesai'),
(14, 7, 30, '2022-03-22', '2022-03-27', '', '4500', '0000-00-00', 'Belum Kembali', 'Belum Selesai');

-- --------------------------------------------------------

--
-- Struktur dari tabel `sepeda`
--

CREATE TABLE `sepeda` (
  `id_sepeda` int(11) NOT NULL,
  `kode_tipe` varchar(120) NOT NULL,
  `merek` varchar(120) NOT NULL,
  `no_plat` varchar(20) NOT NULL,
  `warna` varchar(20) NOT NULL,
  `tahun` varchar(4) NOT NULL,
  `status` enum('0','1') NOT NULL DEFAULT '1',
  `harga` varchar(11) NOT NULL,
  `denda` int(11) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `helm` enum('0','1') NOT NULL DEFAULT '0',
  `tubles` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `sepeda`
--

INSERT INTO `sepeda` (`id_sepeda`, `kode_tipe`, `merek`, `no_plat`, `warna`, `tahun`, `status`, `harga`, `denda`, `gambar`, `helm`, `tubles`) VALUES
(28, 'SPL', 'migo', 'L 233 N', 'Hijau', '2020', '1', '20000', 2000, 'listrik-33.jpg', '1', '0'),
(29, 'SPL', 'VOLTA', 'L 202 N', 'MERAH', '2021', '0', '40000', 4000, 'listrik-12.jpg', '1', '1'),
(30, 'SPM', 'YAMAHA', 'L 45567 WU', 'Special Annivarsary', '2021', '0', '45000', 4500, 'motor-11.jpg', '1', '1'),
(31, 'SPM', 'YAMAHA', 'L  5990 KL', 'ABU-ABU', '2022', '0', '50000', 5000, 'motor-21.jpg', '1', '1'),
(32, 'SPM', 'HONDA', 'L 8990 LK', 'MERAH', '2012', '0', '40000', 2000, 'gl31.jpg', '1', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tipe`
--

CREATE TABLE `tipe` (
  `id_tipe` int(11) NOT NULL,
  `kode_tipe` varchar(120) NOT NULL,
  `nama_tipe` varchar(120) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tipe`
--

INSERT INTO `tipe` (`id_tipe`, `kode_tipe`, `nama_tipe`) VALUES
(11, 'SPL', 'Sepeda listrik'),
(12, 'SP', 'sepeda'),
(13, 'SPM', 'sepeda motor');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `id_transaksi` int(11) NOT NULL,
  `id_customer` int(11) NOT NULL,
  `id_sepeda` int(11) NOT NULL,
  `tgl_rental` date NOT NULL,
  `tgl_kembali` date NOT NULL,
  `harga` varchar(120) NOT NULL,
  `denda` int(11) NOT NULL,
  `total_denda` int(11) DEFAULT NULL,
  `tgl_pengembalian` date DEFAULT NULL,
  `status_pengembalian` enum('0','1') NOT NULL DEFAULT '0',
  `status_rental` enum('belum selesai','selesai') NOT NULL DEFAULT 'belum selesai',
  `bukti_pembayaran` varchar(120) DEFAULT NULL,
  `status_pembayaran` enum('0','1') NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`id_transaksi`, `id_customer`, `id_sepeda`, `tgl_rental`, `tgl_kembali`, `harga`, `denda`, `total_denda`, `tgl_pengembalian`, `status_pengembalian`, `status_rental`, `bukti_pembayaran`, `status_pembayaran`) VALUES
(6, 4, 32, '2022-03-22', '2022-03-25', '40000', 2000, NULL, NULL, '0', 'belum selesai', NULL, '0');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indeks untuk tabel `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_customer`);

--
-- Indeks untuk tabel `rental`
--
ALTER TABLE `rental`
  ADD PRIMARY KEY (`id_rental`);

--
-- Indeks untuk tabel `sepeda`
--
ALTER TABLE `sepeda`
  ADD PRIMARY KEY (`id_sepeda`);

--
-- Indeks untuk tabel `tipe`
--
ALTER TABLE `tipe`
  ADD PRIMARY KEY (`id_tipe`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`id_transaksi`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `customer`
--
ALTER TABLE `customer`
  MODIFY `id_customer` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT untuk tabel `rental`
--
ALTER TABLE `rental`
  MODIFY `id_rental` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `sepeda`
--
ALTER TABLE `sepeda`
  MODIFY `id_sepeda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT untuk tabel `tipe`
--
ALTER TABLE `tipe`
  MODIFY `id_tipe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
