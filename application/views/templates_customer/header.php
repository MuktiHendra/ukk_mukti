<!DOCTYPE html>
<html lang="en">
<link href="<?php echo base_url('assets/assets_cust/css/styles.css')?>" rel="stylesheet" >
<link href="<?= base_url('assets/font-awesome/css/font-awesome.min.css') ?> "rel="stylesheet" />

<nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container px-4 px-lg-5">
                <a class="navbar-brand" href="#!">Rental Sepeda</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                        <li class="nav-item"><a class="nav-link" aria-current="page" href="<?= base_url('customer/dashboard'); ?>">Beranda</a></li>
                        <!-- <li class="nav-item"><a class="nav-link" aria-current="page" href=" <?= base_url('customer/status_rental'); ?>">Status Rental</a></li> -->
                        <!-- <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">Kategori</a>
                            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                <li><a class="dropdown-item" href="#!">Sepeda</a></li>
                                <li><hr class="dropdown-divider" /></li>
                                <li><a class="dropdown-item" href="#!">Sepeda Listrik</a></li>
                                <li><a class="dropdown-item" href="#!">Sepeda Motor</a></li>
                            </ul>
                        </li> -->
<!--                         <li class="nav-item"><a class="nav-link" href="<?= base_url('register') ?>">Register</a></li> -->
                        <?php if($this->session->userdata('nama')){ ?><li class="nav-item"><a class="nav-link" href="<?= base_url('auth/logout'); ?>">Welcome <?= $this->session->userdata('nama'); ?><span> | Logout</span></a>
                        <a class="nav-link" href="<?= base_url('auth/ganti_password'); ?>"><span> | Logout</span></a>
                         </li> <?php }
                        else{ ?>
                        <li class="nav-item"><a class="nav-link" href="<?= base_url('auth/login'); ?>">Login</a></li>
                        <?php } ?></ul>

                    </ul>
                    <form class="d-flex">
                        <a class="btn btn-outline-dark" href="<?= base_url('customer/Transaksi/index'); ?>">
                            <i class="bi-cart-fill me-1"></i>
                            Transaksi
                            <span class="badge bg-dark text-white ms-1 rounded-pill"></span>
                        </a>
                    </form>
                </div>
            </div>
        </nav>

