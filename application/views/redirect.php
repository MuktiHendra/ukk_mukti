<?php


if(empty($this->session->userdata('username'))){
    $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
      <strong>Anda belum login!</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>');
    redirect('customer/dashboard');
  }
  elseif($this->session->userdata('role_id') != '1'){
    $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
      <strong>Anda tidak punya akses ke halaman ini!</strong>
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>');
    redirect('customer/dashboard');
} else {
      redirect('customer/dashboard');
  }