<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Form Update Data Sepeda</h1>
    </div>

    <div class="card">
      <div class="card-body">

        <?php foreach($sepeda as $sp): ?>

        <form action="<?= base_url('admin/data_sepeda/update_sepeda_aksi') ?>" enctype="multipart/form-data" method="post">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="">Tipe Sepeda</label>
                <input type="hidden" name="id_sepeda" value="<?= $sp->id_sepeda; ?>">
                <select name="kode_tipe" id="" class="form-control">
                  <option value="<?= $sp->kode_tipe ?>"><?= $sp->nama_tipe ?></option>
                  <?php foreach($tipe as $tp): ?>
                  <option value="<?= $tp->kode_tipe ?>"><?= $tp->nama_tipe; ?></option>
                  <?php endforeach; ?>
                </select>
                </div>

              <div class="form-group">
                <label for="">Merek</label>
                <input type="text" name="merek" class="form-control" value="<?= $sp->merek ?>">
                </div>

              <div class="form-group">
                <label for="">Nomor Plat</label>
                <input type="text" name="no_plat" class="form-control" value="<?= $sp->no_plat ?>">
                </div>

              <div class="form-group">
                <label for="">Warna</label>
                <input type="text" name="warna" class="form-control" value="<?= $sp->warna ?>">
                </div>
  
              <div class="form-group">
                <label for="">Helm</label>
                <select name="helm" id="" class="form-control">
                  <option <?php if($sp->helm == "1"){echo "selected='selected'";} echo $sp->helm; ?> value="1">Tersedia</option>
                  <option <?php if($sp->helm == "0"){echo "selected='selected'";} echo $sp->helm; ?> value="0">Tidak Tersedia</option>
                </select>
                </div>
              
              <div class="form-group">
                <label for=""> Ban Tubles </label>
                <select name="tubles" id="" class="form-control">
                  <option <?php if($sp->tubles == "1"){echo "selected='selected'";} echo $sp->tubles; ?> value="1">Tubles</option>
                  <option <?php if($sp->tubles == "0"){echo "selected='selected'";} echo $sp->tubles; ?> value="0">Tidak Tubles</option>
                </select>
                </div>
            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="">Tahun</label>
                <input type="text" name="tahun" class="form-control" value="<?= $sp->tahun ?>">
                </div>

              <div class="form-group">
                <label for="">Harga</label>
                <input type="nusper" name="harga" class="form-control" value="<?= $sp->harga ?>">
                </div>
              <div class="form-group">
                <label for="">Denda</label>
                <input type="nusper" name="denda" class="form-control" value="<?= $sp->denda ?>">
                </div>

              <div class="form-group">
                <label for="">Status</label>
                <select name="status" id="" class="form-control">
                  <option <?php if($sp->status == "1"){echo "selected='selected'";} echo $sp->status; ?> value="1">Tersedia</option>
                  <option <?php if($sp->status == "0"){echo "selected='selected'";} echo $sp->status; ?> value="0">sedang di rental</option>
                </select>
                </div>
              
              <div class="form-group">
                <label for="">Gambar</label>
                <input type="file" name="gambar" class="form-control">
              </div>

              <button type="submit" class="btn btn-primary mt-4">Simpan</button>
              <button type="reset" class="btn btn-success mt-4">Reset</button>
            </div>
          </div>
        </form>

        <?php endforeach; ?>
      </div>
    </div>

  </section>
</div>