<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Data sepeda</h1>
    </div>
    
    <a href="<?= base_url('admin/data_sepeda/tambah_sepeda'); ?>" class="btn btn-primary mb-3">Tambah Data</a>
    <?= $this->session->flashdata('pesan'); ?>

    <table class="table table-hover table-striped table-bordered">
      <thead>
        <tr>
          <th>No</th>
          <th>Gambar</th>
          <th>Tipe</th>
          <th>Merek</th>
          <th>Nomor Plat</th>
          <th>Status</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $no = 1;
        foreach($sepeda as $sp): ?>
        <tr>
          <td><?= $no++; ?>.</td>
          <td>
            <img width="70px;" src="<?= base_url('assets/upload/'). $sp->gambar; ?>" alt="">
          </td>
          <td><?= $sp->kode_tipe; ?></td>
          <td><?= $sp->merek; ?></td>
          <td><?= $sp->no_plat; ?></td>
          <td>
            <?php if($sp->status == 0){ ?>
              <span class="badge badge-danger">sedang dirental</span>
            <?php }
            else{ ?>
              <span class="badge badge-primary">Tersedia</span>
            <?php } ?>
          </td>
          <td>
            <a href="<?= base_url('admin/data_sepeda/detail_sepeda/'). $sp->id_sepeda; ?>" class="btn btn-sm btn-success"><i class="fas fa-eye"></i></a>
            <a onclick="return confirm('Yakin hapus?')" href="<?= base_url('admin/data_sepeda/delete_sepeda/'). $sp->id_sepeda; ?>" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></a>
            <a href="<?= base_url('admin/data_sepeda/update_sepeda/'). $sp->id_sepeda; ?>" class="btn btn-sm btn-primary"><i class="fas fa-edit"></i></a>
          </td>
        </tr>
        <?php endforeach; ?>
      </tbody>
    </table>



  </section>
</div>