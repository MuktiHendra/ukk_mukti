<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Form Input Data Sepeda</h1>
    </div>

    <div class="card">
      <div class="card-body">

        <form method="POST" action="<?= base_url('admin/Data_sepeda/tambah_sepeda_aksi') ?>" enctype="multipart/form-data" method="post">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label for="">Tipe Sepeda</label>
                <select name="kode_tipe" id="kode_tipe" class="form-control">
                  <option value="">--Pilih Tipe Sepeda--</option>
                  <?php foreach($tipe as $tp): ?>
                  <option value="<?= $tp->kode_tipe ?>"><?= $tp->nama_tipe; ?></option>
                  <?php endforeach; ?>
                </select>
               <?= form_error('kode_tipe', '<div class="text-small text-danger">', '</div>') ?>
              </div>

              <div class="form-group">
                <label for="">Merek</label>
                <input type="text" name="merek" id="merek" class="form-control">
               
              </div>

              <div class="form-group">
                <label for="">Nomor Plat</label>
                <input type="text" name="no_plat" id="no_plat" class="form-control">
                
              </div>

              <div class="form-group">
                <label for="">Warna</label>
                <input type="text" name="warna" id="warna" class="form-control">
                
              </div>

            </div>
            <div class="col-md-6">
              <div class="form-group">
                <label for="">Harga Sewa perhari</label>
                <input type="number" name="harga" id="harga" class="form-control">
                
              </div>

              <div class="form-group">
                <label for="">Denda</label>
                <input type="number" name="denda" id="denda" class="form-control">
                
              </div>

             <div class="form-group">
                <label for="">Helm</label>
                <select name="helm" id="helm" class="form-control">
                  <option value="">--Pilih--</option>
                  <option value="1">Tersedia</option>
                  <option value="0">Tidak Tersedia</option>
                </select>
                
              </div>
              
              <div class="form-group">
                <label for="">Ban Tubles</label>
                <select name="tubles" id="tubles" class="form-control">
                  <option value="">--Pilih--</option>
                  <option value="1">Tubles</option>
                  <option value="0">Tidak Tubles</option>
                </select>
                
              </div> 

              <div class="form-group">
                <label for="">Tahun</label>
                <input type="text" name="tahun" id="tahun" class="form-control">
               
              </div>

              <div class="form-group">
                <label for="">Status</label>
                <select name="status" id="status" class="form-control">
                  <option value="">--Pilih Status--</option>
                  <option value="1">Tersedia</option>
                  <option value="0">sedang di rental</option>
                </select>
               
              </div>
              
              <div class="form-group">
                <label for="">Gambar</label>
                <input type="file" name="gambar" id="gambar" class="form-control">
              </div>

              <button type="submit" class="btn btn-primary mt-4">Simpan</button>
              <button type="reset" class="btn btn-success mt-4">Reset</button>
            </div>
          </div>
        </form>
      </div>
    </div>


  </section>
</div>