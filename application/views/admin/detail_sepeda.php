<div class="main-content">
  <section class="section">
    <div class="section-header">
      <h1>Detail sepeda</h1>
    </div>
  </section>

  <?php foreach($detail as $dt): ?>
    <div class="card">
      <div class="card-body">
        <div class="row">
          <div class="col-md-5">
            <img width="110%;" src="<?= base_url('assets/upload/'). $dt->gambar; ?>" alt="">
          </div>
          <div class="col-md-7">
            <table class="table">
              <tr>
                <td>Tipe Sepeda</td>
                <td>
                  <?php 
                    if($dt->kode_tipe == "SPL"){
                      echo "Sepeda Listrik";
                    }
                    elseif($dt->kode_tipe == "SPM"){
                      echo "Sepeda Motor";
                    }
                    elseif($dt->kode_tipe == "SP"){
                      echo "Sepeda";
                    }
                    else{ ?>
                      <span class="text-danger">Tipe sepeda belum terdaftar</span>
                    <?php }
                  ?>
                </td>
              </tr>
              <tr>
                <td>Merek</td>
                <td><?= $dt->merek; ?></td>
              </tr>
              <tr>
                <td>No. Plat</td>
                <td><?= $dt->no_plat; ?></td>
              </tr>
              <tr>
                <td>Warna</td>
                <td><?= $dt->warna; ?></td>
              </tr>
              <tr>
                <td>Tahun</td>
                <td><?= $dt->tahun; ?></td>
              </tr>
              <tr>
                <td>Harga Sewa</td>
                <td>Rp. <?= number_format($dt->harga, 0, ',', '.'); ?>,-</td>
              </tr>
              <tr>
                <td>Denda</td>
                <td>Rp. <?= number_format($dt->denda, 0, ',', '.'); ?>,-</td>
              </tr>
              <tr>
                <td>Status</td>
                <td>
                  <?php
                  if($dt->status == "1"){ ?>
                    <span class="badge badge-danger">Tidak Tersedia</span>                 
                  <?php }
                  else{ ?>
                    <span class="badge badge-primary">Tersedia</span>
                  <?php } ?>
                </td>
              </tr>

              <tr>
                <td>Helm</td>
                <td>
                  <?php
                  if($dt->helm == "0"){ ?>
                    <span class="badge badge-danger">Tidak Tersedia</span>                 
                  <?php }
                  else{ ?>
                    <span class="badge badge-primary">Tersedia</span>
                  <?php } ?>
                </td>
              </tr>
              <tr>
                <td>Ban Tubles</td>
                <td>
                  <?php
                  if($dt->tubles == "1"){ ?>
                    <span class="badge badge-danger">Tubles</span>                 
                  <?php }
                  else{ ?>
                    <span class="badge badge-primary">Tidak Tubles</span>
                  <?php } ?>
                </td>
              </tr>
            </table>

            <a href="<?= base_url('admin/data_sepeda'); ?>" class="btn btn-sm btn-danger ml-4">Kembali</a>
            <a href="<?= base_url('admin/data_sepeda/update_sepeda/').$dt->id_sepeda; ?>" class="btn btn-sm btn-primary">Update</a>
          </div>
        </div>
      </div>
    </div>

  <?php endforeach; ?>
</div>