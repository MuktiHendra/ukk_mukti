
<div class="container mt-5 mb-5">
	<div class="row">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header alert alert-success">
					Invoice pembayaran anda
				</div>
				<div class="card-body">
					<table class="table">
						<?php foreach( $transaksi as $item ){ ?>
						<tr>
							<th>Merk Sepeda</th>
							<td>:</td>
							<td><?= $item->merek ?></td>	
						</tr>
						<tr>
							<th>Tanggal Rental</th>
							<td>:</td>
							<td><?= $item->tgl_rental ?></td>	
						</tr>
						<tr>
							<th>Tanggal Kembali </th>
							<td>:</td>
							<td><?= $item->tgl_kembali ?></td>	
						</tr>
						<tr>
							<th>Harga Sewa/Hari</th>
							<td>:</td>
							<td><?= number_format($item->harga,0,'.',',') ?></td>	
						</tr>
						<tr>
							<?php 
								$x = strtotime($item->tgl_kembali);
								$y = strtotime($item->tgl_rental);

								$j = abs($y - $x);

								 $jf = $j/86400;  // 86400 seconds in one day

								// // and you might want to convert to integer
								 $jf = intval($jf);

								 if($jf <= 0){
								 	$jf = 1;
								 } else {
								 	$jf += 1;
								 }
							 ?>
							<th>jumlah Hari Sewa</th>
							<td>:</td>
							<td><?= $jf ?> Hari</td>	
						</tr>
						<tr class="text-success">
							<th>Harga Total</th>
							<td>:</td>
							<td><button class="btn btn-sm btn-success">Rp <?= number_format($item->harga*$jf,0,',','.') ?></button></td>	
						</tr>
						<tr>
							<td></td>
							<td></td>
							<td><a href="<?= base_url('customer/Transaksi/invoice') ?>" class="btn btn-sm btn-secondary">Print Invoice</a></td>
						</tr>

						<?php } ?>
					</table>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="card">
			<div class="card-header alert alert-primary">
				informasi pembayaran
			</div>
			<div class="card-body">
				<p class="text-success">silahkan melakukan pembayaran melalui nomor rekening di bawah ini : </p>
				<ul class="list-group list-group-flush">
					<li class="list-group-item">Bank mandiri 12134234908945</li>
					<li class="list-group-item">Bank BCA 22134234908945</li>
					<li class="list-group-item">Bank BNI 32334214908945</li>

					<!-- demo modal tidak muncul -->
					<?php 

							if(empty($item->bukti_pembayaran)) { ?>
						<!-- Button trigger modal -->
							<button style="width: 100%" type="button" class="btn btn-sm btn-danger mt-3" data-bs-toggle="modal" data-bs-target="#exampleModal">
							  Upload Bukti pembayaran
							</button>
					<?php }elseif($item->status_pembayaran == '0'){ ?>
						<button style="width: 100%" class="btn btn-sm btn-warning">
							<i class="fa fa-clock-o" aria-hidden="true" style="color: black;"></i>Menunggu Konfirmasi
						</button>
					<?php }elseif($item->status_pembayaran == '1'){ ?>
						<button style="width: 100%" class="btn btn-sm btn-success">
							Pembayaran selesai
						</button>
					<?php } ?>
				</ul>
			</div>	
		</div>
	   	</div>
	</div>
</div>


<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Upload Bukti Pembayaran Anda</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form  method="POST" action="<?= base_url('customer/Transaksi/pembayaran_aksi') ?>" enctype="multipart/form-data" >
      <div class="modal-body">
        <div class="form-group">
        	<label>Upload Bukti Pembayaran</label>
        	<input type="hidden" name="id_transaksi" class="form-control" value="<?= $item->id_transaksi ?>">
        	<input type="file" name="gambar" class="form-control">
        </div>
      </div>
      <div class="modal-footer">
       <!--  <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> -->
        <button type="sumbit" class="btn btn-sm btn-success">Kirim</button>
      </div>
      </form>
    </div>
  </div>
</div>