<!-- <?php $no=1; ?> -->
<div class="container mt-5 mb-5 ">
	<div class="row">
		<div class="col-md-8">
			<dic class="card">
				<div class="card-header">
					Profil Anda
				</div>
				<div class="card-body">
					<tr>
						<th>No</th>
						<td>:</td>
						<td><?= $no++?></td>	
					</tr>
					<tr>
						<th>Nama</th>
						<td>:</td>
						<td><?= $customer->nama?></td>	
					</tr>
					<tr>
						
					</tr>
				</div>
			</dic>
		</div>
	</div>
</div>

<div class="container">
	<div class="card mx-auto" style="margin-top: 120px; margin-bottom: 150px; width: 80% ">
		<div class="card-header">
			Data Transaksi anda
		</div>

		<span class="mt-2 p-2"><?php echo $this->session->flashdata('pesan') ?></span>
		<div class="card-body">
			<table class="table table-bordered table-stripped">
				<!-- <thead class="thead-dark"> -->
				<tr>
					<!-- <th scope="col">id transaksi</th> -->
					<th>No</th>
					<th>Nama Customer</th>
					<th>Merk sepeda</th>
					<!-- <th scope="col">Status</th> -->
					<th>No plat</th>
					<th>harga Sewa</th>					
					<th>aksi</th>
				</tr>
				<?php 
				$no = 1;
				foreach( $transaksi as $item) { ?>
				<tr>
					<td><?= $no++; ?></td>
					<td><?=	$item->nama ?></td>
					<td><?=	$item->merek ?></td>
					<td><?=	$item->no_plat ?></td>
					<td>Rp. <?=	number_format($item->harga,0,',','.') ?></td>
					<td>
						<?php if($item->status_rental == "selesai") { ?>
							<button class="btn btn-sm btn-secondary ">Rental Selesai</button>
							<a href="<?= base_url('customer/Transaksi/pembayaran/'.$item->id_transaksi)  ?>" class="btn btn-sm btn-primary">cetak invoice</a>
						<?php }else{ ?>
							<a href="<?= base_url('customer/Transaksi/pembayaran/'.$item->id_transaksi)  ?>" class="btn btn-sm btn-success">cek pembayaran</a>
						<?php } ?> 
					</td>
				</tr>
				<?php } ?>
			</table>
		</div>
	</div>
</div>