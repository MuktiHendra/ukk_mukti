<div class="container">

	<div class="card" style="margin-top: 50px; margin-bottom: 300px;">
		<div class="card-header">
			From Rental			
		</div>
		<div class="card-body">
			<?php foreach($detail as $dt) :  ?>
			<form method="POST" action="<?php echo base_url('customer/rental/tambah_rental_aksi') ?>">
				
				<div class="input-group mb-3">
					<label class="col-2">Harga Sewa/Hari</label>
					<input type="hidden" name="id_sepeda" value="<?php echo $dt->id_sepeda ?>">
					<input type="text" name="harga" class="form-control" id="harga" value="<?php echo $dt->harga ?>" readonly>
				</div>
				<div class="input-group mb-3">
					<label class="col-2">Denda/Hari</label>
					<input type="text" name="denda" class="form-control" value="<?php echo $dt->denda ?>" readonly>
				</div>
				<div class="input-group mb-3">
					<label class="col-2">Tanggal Rental</label>
					<input type="date" min="<?= date('Y-m-d'); ?>" name="tanggal_rental" class="form-control">
				</div>
				<div class="input-group mb-3">
					<label class="col-2" >Tanggal Kembali</label>
					<input type="date" min="<?= date('Y-m-d'); ?>" id="tg" name="tanggal_kembali" class="form-control">
				</div>
				
	
				<button type="sumbit" class="btn-btn-warning">Rental</button>
					
				
			</form>
		<?php endforeach; ?>
		</div>
	</div>
</div>	
