<!DOCTYPE html>
<html lang="en">
    <body>
        <!-- head-->
     <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>RENTAL</title>
        <!-- Favicon-->
        <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
        <!-- Bootstrap icons-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css" rel="stylesheet" />
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="<?php echo base_url()?>/assets/assets_cust/css/styles.css" rel="stylesheet" />
    </head>
        
        <!-- Header-->

        <header class="py-5" style="background-image: url('../upload/back.jpg');background-repeat:no-repeat;background-size:100% 100%; ">
            <div class="container px-4 px-lg-5 my-5">
                <div class="text-center text-white">
                    <h1 class="display-4 fw-bolder" style=" color: black;">RENTAL </h1>
                    <p class="lead fw-normal text-white-50 mb-0"></p>
                </div>
            </div>
        </header>
        <!-- Section-->
        <section class="py-5">
            <div class="container px-4 px-lg-5 mt-5">
                <div class="row gx-4 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-4 justify-content-center">
                    <?php
                    foreach ($sepeda as $sp) : ?>
                    <div class="col mb-5">
                        <div class="card h-100">
                            <!-- Product image-->
                            <div style="width: 100%; height: 300px; background: url(<?php echo base_url('assets/upload/'.$sp->gambar) ?>) center center/contain no-repeat;"></div>
                            <!-- Product details-->
                            <div class="card-body p-4">
                                <div class="text-center">
                                    <!-- Product name-->
                                    <h2 class="fw-bolder">
                                        <p><?php echo $sp->merek ?></p>
                                    </h2>
                                    <h4>Rp<?= number_format($sp->harga, 0, ',', '.'); ?> / Hari</h4>

                                    <!-- <h5>No. Plat : <?php echo $sp->no_plat ?></h5> -->
                                    <ul class="Bicyle-info-list">
                                        <li>helm</li>
                                        <li>tubles</li>
                                    </ul>
                                   
                                </div>
                            </div>
                            <!-- Product actions-->
                            <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">

                                <?php 
                                if ($sp->status == 0) {
                                     echo "<span class='btn btn-danger disable'>sedang di rental</span>";
                                }else{
                                    echo anchor('customer/rental/tambah_rental/'.$sp->id_sepeda,'<button class="btn btn-success">Rental</button>');
                                }
                                    
                                 ?>
                                <a class="btn btn-warning" href="<?php echo  base_url('customer/dashboard/detail_sepeda/').$sp->id_sepeda ?>">Detail</a>
                                
                            </div>
                        </div>
                    </div>
                 <?php endforeach; ?>
                </div>
            </div>
        </section>
        <!-- Footer-->
        
        <!-- Bootstrap core JS-->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
    </body>
</html>
