<?php 
	$data = $data[0];
	$startTimeStamp = strtotime($data->tgl_rental);
$endTimeStamp = strtotime($data->tgl_kembali);

$timeDiff = abs($endTimeStamp - $startTimeStamp);

$numberDays = $timeDiff/86400;  // 86400 seconds in one day

// and you might want to convert to integer
$numberDays = intval($numberDays);

if($numberDays <= 0){
	$numberDays = 1;
} else {
	$numberDays += 1;
}
?>

<!-- ini print -->
<h3 style="text-align: center;">Laporan Transaksi Rental sepeda</h3>



<table class="table table-bordered table-striped mt-3">
  <tr>
	<th>transaksi</th>
	<th>Tanggal Rental</th>
	<th>Tanggal Kembali</th>
	<th>Harga Sepeda</th>
	<th>Total Hari</th>
</tr>

  <tr>
	<td>
	<?=$data->id_transaksi?>
	</td>
	<td><?=$data->tgl_rental?></td>
	<td><?=$data->tgl_kembali?></td>
	<td><?=$data->harga?></td>
	<td><?=$numberDays?></td>
  </tr>

  <tr>
	<td colspan="4">
		<p>
		Sub Total<br>
		Denda<br>
		</p>
		<h5 class="text-success"><strong>Grand Total</strong></h5>
	</td>			
	<td>
		<p>
		<?= $data->harga * $numberDays?><br>
		<?= $data->total_denda ?><br>
		</p>
<h5 class="text-success"><strong><?= $data->harga * $numberDays + $data->total_denda?></strong></h5>
	</td>
  </tr>

<script>
  window.print();
</script>