<div class="container" style="margin-top: 50px; margin-bottom: 300px;">
	
	<div class="card">
		<div class="card-body">
			<?php foreach ($detail as $dt ) : ?>
				<div class="row">
					<div class="col-md-6">
						<img src="<?php echo base_url('assets/upload/'.$dt->gambar) ?>">
					</div>
					<div class="col-md-6">
			<table class="table">
              <tr>
                <td>Tipe Sepeda</td>
                <td>
                  <?php 
                  
                    if($dt->kode_tipe == "SPL"){
                      echo "Sepeda Listrik";
                    }
                    elseif($dt->kode_tipe == "SPM"){
                      echo "Sepeda Motor";
                    }
                    elseif($dt->kode_tipe == "SP"){
                      echo "Sepeda";
                    }
                    else{ ?>
                      <span class="text-danger">Tipe sepeda belum terdaftar</span>
                    <?php }
                  ?>
                </td>
              </tr>
              <tr>
                <td>Merek</td>
                <td><?= $dt->merek; ?></td>
              </tr>
              <tr>
                <td>No. Plat</td>
                <td><?= $dt->no_plat; ?></td>
              </tr>
              <tr>
                <td>Warna</td>
                <td><?= $dt->warna; ?></td>
              </tr>
              <tr>
                <td>Tahun</td>
                <td><?= $dt->tahun; ?></td>
              </tr>
              <tr>
                <td>Harga Sewa</td>
                <td>Rp. <?= number_format($dt->harga, 0, ',', '.'); ?>,-</td>
              </tr>
              <tr>
                <td>Denda</td>
                <td>Rp. <?= number_format($dt->denda, 0, ',', '.'); ?>,-</td>
              </tr>
              <tr>
                <td>Status</td>
                <td>
                  <?php
                  if($dt->status == "1"){ 
                  	echo "Tersedia";  
                  }
                  else{ 
                    echo "Tidak Tersedia";
                   } ?>
                </td>
              </tr>

              <tr>
                <td>Helm</td>
                <td>
                	 <?php
                  if($dt->helm == "1"){ 
                  	echo "Tersedia";  
                  }
                  else{ 
                    echo "Tidak Tersedia";
                   } ?>
                </td>
              </tr>
              <tr>
                <td>Ban Tubles</td>
                <td>
                   <?php
                  if($dt->tubles == "1"){ 
                  	echo "Tersedia";  
                  }
                  else{ 
                    echo "Tidak Tersedia";
                   } ?>
                </td>
              </tr>
            </table>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
</div>