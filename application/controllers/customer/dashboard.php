	<?php

class Dashboard extends CI_Controller{

  public function __construct(){
    parent::__construct();
    
    if(empty($this->session->userdata('username'))){
      $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Anda belum login!</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>');
      redirect('auth/login');
    }
    elseif($this->session->userdata('role_id') == '1'){
      redirect('admin/dashboard');
    }
  }

  public function index(){

    $data['sepeda'] = $this->rental_model->get_data('sepeda')->result();
    // var_dump($data);
    // die();
   	$this->load->view('templates_customer/header');
    $this->load->view('customer/dashboard', $data);
    $this->load->view('templates_customer/footer');
  }
  public function detail_sepeda($id){
    $data['detail'] = $this->rental_model->ambil_id_sepeda($id);
    // var_dump($data);
    // die();
   	$this->load->view('templates_customer/header');
    $this->load->view('customer/detail_sepeda', $data);
    $this->load->view('templates_customer/footer');
  }


}
?>