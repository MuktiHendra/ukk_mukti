<?php 

Class Rental extends CI_Controller
{
	 public function __construct(){
        parent::__construct();
        
        if(empty($this->session->userdata('username'))){
          $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
            <strong>Anda belum login!</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>');
          redirect('auth/login');
        }
      }
	public function tambah_rental($id)
	{

	$data['detail'] = $this->rental_model->ambil_id_sepeda($id);
	$this->load->view('templates_customer/header');
    $this->load->view('customer/tambah_rental', $data);
    $this->load->view('templates_customer/footer');
	}
	public function tambah_rental_aksi()
	{
		$id_customer		= $this->session->userdata('id_customer');
		$id_sepeda 			= $this->input->post('id_sepeda');
		$tanggal_rental		= $this->input->post('tanggal_rental');
		$tanggal_kembali 	= $this->input->post('tanggal_kembali');
		$denda	 	 		= $this->input->post('denda');
		$harga	 	 		= $this->input->post('harga');
		
	
		$data = array(
			'id_customer' 				=> $id_customer,
			'id_sepeda'	  				=> $id_sepeda,
			'tgl_rental'				=> $tanggal_rental,
			'tgl_kembali'				=> $tanggal_kembali,
			'denda'						=> $denda,
			'harga'						=> $harga,
			'tgl_pengembalian'			=> '-',
			'status_rental'				=> 'Belum Selesai',
			// 'status_pengembalian'		=> 'Belum Kembali',
		);


		$this->rental_model->insert_data($data,'transaksi');

		$status = array(
			'status' => '0'
		); 

		$id = array( 
			'id_sepeda' =>$id_sepeda
	    );

		$this->rental_model->update_data('sepeda',$status,$id);
		$this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
      	transaksi berhasil, checkout!.
      	<button type="button" class="close" data-dismiss="alert" aria-label="close">
        <span aria-hidden="true">&times;</span>
      	</button></div>');
	redirect('customer/dashboard');
	}
	
}

 ?>