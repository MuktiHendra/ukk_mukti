<?php

class userTransaksi extends CI_Controller
{
  public function __construct()
  {
    parent::__construct();

    if (empty($this->session->userdata('username'))) {
      $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Anda belum login!</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>');
      redirect('auth/login');
    }
  }

  public function index()
  {

    $id = $this->session->userdata('id_customer');
    $where = ['id_customer' => $id];
    $data['transaksi'] = $this->rental_model->get_where($where, 'transaksi')->result();

    foreach ($data['transaksi'] as $item) {
      $dimana = ['id_sepeda' => $item->id_sepeda];
      $item->sepeda = $this->rental_model->get_where($dimana, 'sepeda')->result();
    }

    // for ($i=0; $i < count($data['transaksi']); $i++) { 
    //   $dimana = ['id_sepeda' => $item->id_sepeda]
    //   $item['sepeda'] = $this->rental_model->get_where($dimana, 'sepeda')->result();
    // }
    $this->load->view('templates_customer/header');
    $this->load->view('customer/userTransaksi', $data);
    $this->load->view('templates_customer/footer');
  }
  public function userTransaksi_aksi()
  {
    $this->load->helper('string');
    var_dump(random_string('alnum', 35));
    $target_dir = realpath(dirname(getcwd())) . "/rental_ukk/upload/";
    $target_file = $target_dir . basename($_FILES["gambar"]["name"]);
    $uploadOk = 1;
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

    // Check if image file is a actual image or fake image
    if (isset($_POST["gambar"])) {
      $check = getimagesize($_FILES["gambar"]["tmp_name"]);
      if ($check !== false) {
        echo "File is an image - " . $check["mime"] . ".";
        $uploadOk = 1;
      } else {
        echo "File is not an image.";
        $uploadOk = 0;
      }
    }

    // Check if file already exists
    if (file_exists($target_file)) {
      echo "Sorry, file already exists.";
      $uploadOk = 0;
    }

    // Check file size
    if ($_FILES["gambar"]["size"] > 500000) {
      echo "Sorry, your file is too large.";
      $uploadOk = 0;
    }

    // Allow certain file formats
    if (
      $imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
      && $imageFileType != "gif"
    ) {
      echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
      $uploadOk = 0;
    }

    // Check if $uploadOk is set to 0 by an error
    if ($uploadOk == 0) {
      echo "Sorry, your file was not uploaded.";
      // if everything is ok, try to upload file
    } else {
      $lolo = random_string('alnum', 35) . '.' . $imageFileType;
      $oi = $target_dir . $lolo;
      if (move_uploaded_file($_FILES["gambar"]["tmp_name"], $oi)) {
        $this->db->query("UPDATE `transaksi` SET `bukti_pembayaran` = '$lolo' , `status_pembayaran`= '1'  WHERE `id_transaksi` = " . $_POST['id_transaksi']);



        redirect('customer/userTransaksi');
      } else {
        echo "Sorry, there was an error uploading your file.";
      }
    }
  }

  public function invoice($id)
  {
    $data['data'] = $this->db->query("SELECT * FROM `transaksi` INNER JOIN `sepeda` ON transaksi.id_sepeda = sepeda.id_sepeda WHERE `id_transaksi` = $id")->result();
    $this->load->view('templates_customer/header');
    $this->load->view('customer/invoice', $data);
    // $this->load->view('templates_customer/footer');
  }
}
