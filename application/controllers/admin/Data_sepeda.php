<?php

class Data_sepeda extends CI_Controller{

  public function __construct(){
    parent::__construct();
    
    if(empty($this->session->userdata('username'))){
      $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Anda belum login!</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>');
      redirect('auth/login');
    }
    elseif($this->session->userdata('role_id') != '1'){
      $this->session->set_flashdata('pesan', '<div class="alert alert-danger alert-dismissible fade show" role="alert">
        <strong>Anda tidak punya akses ke halaman ini!</strong>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>');
      redirect('customer/dashboard');
    }
  }

  public function index(){
    $data['sepeda'] = $this->rental_model->get_data('sepeda')->result();
    $data['tipe'] = $this->rental_model->get_data('tipe')->result();
    $this->load->view('templates_admin/header');
    $this->load->view('templates_admin/sidebar');
    $this->load->view('admin/data_sepeda', $data);
    $this->load->view('templates_admin/footer');
  }

  public function tambah_sepeda(){
    $data['tipe'] = $this->rental_model->get_data('tipe')->result();
    $this->load->view('templates_admin/header');
    $this->load->view('templates_admin/sidebar');
    $this->load->view('admin/form_tambah_sepeda', $data);
    $this->load->view('templates_admin/footer');
  }

  public function tambah_sepeda_aksi(){
    $this->_rules();

   if($this->form_validation->run() == TRUE){
    
    
      $kode_tipe    = $this->input->post('kode_tipe');
      $merek        = $this->input->post('merek');
      $no_plat      = $this->input->post('no_plat');
      $warna        = $this->input->post('warna');
      $tahun        = $this->input->post('tahun');
      $status       = $this->input->post('status');
      $harga        = $this->input->post('harga');
      $denda        = $this->input->post('denda');
      $helm         = $this->input->post('helm');
      $tubles       = $this->input->post('tubles');
      $gambar       = $_FILES['gambar']['name'];



       if($gambar!=''){
        $config['upload_path'] = './assets/upload';
        $config['allowed_types'] = 'jpg|jpeg|png|tiff';

        $this->load->library('upload', $config);
        if(!$this->upload->do_upload('gambar')){
          echo "Gambar sepeda gagal diupload";
        }
        else{
          $gambar = $this->upload->data('file_name');
        }
      } 
      $data = array(
        'kode_tipe'    => $kode_tipe,
        'merek'        => $merek,
        'no_plat'      => $no_plat,
        'tahun'        => $tahun,
        'warna'        => $warna,
        'status'       => $status,
        'harga'        => $harga,
        'denda'        => $denda,
        'helm'         => $helm,
        'tubles'       => $tubles,
        'gambar'       => $gambar,
      );

      $this->rental_model->insert_data($data, 'sepeda');
      $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
      Data berhasil ditambahkan
      <button type="button" class="close" data-dismiss="alert" aria-label="close">
        <span aria-hidden="true">&times;</span>
      </button></div>');
      redirect('admin/data_sepeda');
    }else{
       $this->tambah_sepeda();
    }
  }

  public function update_sepeda($id){
    $where = array('id_sepeda' => $id);
    $data['sepeda'] = $this->db->query("SELECT * FROM sepeda sp, tipe tp WHERE sp.kode_tipe = tp.kode_tipe AND sp.id_sepeda = '$id'")->result();
    $data['tipe'] = $this->rental_model->get_data('tipe')->result();
    $this->load->view('templates_admin/header');
    $this->load->view('templates_admin/sidebar');
    $this->load->view('admin/form_update_sepeda', $data);
    $this->load->view('templates_admin/footer');
  }

  public function update_sepeda_aksi(){
    $this->_rules();
   
    if($this->form_validation->run() == FALSE){
      $id = $this->input->post('id_sepeda');
      $this->update_sepeda($id);
    }
    else{
      $id           = $this->input->post('id_sepeda');
      $kode_tipe    = $this->input->post('kode_tipe');
      $merek        = $this->input->post('merek');
      $no_plat      = $this->input->post('no_plat');
      $warna        = $this->input->post('warna');
      $tahun        = $this->input->post('tahun');
      $status       = $this->input->post('status');
      $harga        = $this->input->post('harga');
      $denda        = $this->input->post('denda');
      $helm         = $this->input->post('helm');
      $tubles       = $this->input->post('tubles');
      $gambar       = $_FILES['gambar']['name'];

      if($gambar){
        $config['upload_path'] = './assets/upload';
        $config['allowed_types'] = 'jpg|jpeg|png|tiff';

        $this->load->library('upload', $config);
        
        if($this->upload->do_upload('gambar')){
          $gambar = $this->upload->data('file_name');
          $this->db->set('gambar', $gambar);
        }
        else{
          echo $this->upload->display_error();
        }
      }
      $data = [
        'kode_tipe'    => $kode_tipe,
        'merek'        => $merek,
        'no_plat'      => $no_plat,
        'tahun'        => $tahun,
        'warna'        => $warna,
        'status'       => $status,
        'harga'        => $harga,
        'denda'        => $denda,
        'helm'         => $helm,
        'tubles'       => $tubles,
        'gambar'       => $gambar,
       
      ];

      $where = array('id_sepeda' => $id);

      $this->rental_model->update_data('sepeda', $data, $where);
      $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
      Data berhasil diupdate
      <button type="button" class="close" data-dismiss="alert" aria-label="close">
        <span aria-hidden="true">&times;</span>
      </button></div>');
      redirect('admin/data_sepeda');
      

    }
  }

  public function detail_sepeda($id){
    $data['detail'] = $this->rental_model->ambil_id_sepeda($id);
   
    $this->load->view('templates_admin/header');
    $this->load->view('templates_admin/sidebar');
    $this->load->view('admin/detail_sepeda', $data);
    $this->load->view('templates_admin/footer');
  }

  public function delete_sepeda($id){
    $where = array('id_sepeda' => $id);

    $this->rental_model->delete_data($where, 'sepeda');
    $this->session->set_flashdata('pesan', '<div class="alert alert-success alert-dismissible fade show" role="alert">
    Data berhasil dihapus
    <button type="button" class="close" data-dismiss="alert" aria-label="close">
      <span aria-hidden="true">&times;</span>
    </button></div>');
    redirect('admin/data_sepeda');

  }

  public function _rules(){
    $this->form_validation->set_rules('kode_tipe', 'Kode Tipe', 'required');
    //$this->form_validation->set_rules('merek', 'Merek', 'required');
   // $this->form_validation->set_rules('no_plat', 'Nomor Plat', 'required');
    //$this->form_validation->set_rules('tahun', 'Tahun', 'required');
   // $this->form_validation->set_rules('warna', 'Warna', 'required');
   // $this->form_validation->set_rules('status', 'Status', 'required');
   // $this->form_validation->set_rules('harga', 'Harga', 'required');
   // $this->form_validation->set_rules('denda', 'Denda', 'required');
    //$this->form_validation->set_rules('helm', 'helm', 'required');
   // $this->form_validation->set_rules('tubles', 'tubles', 'required');
  }


}